import Link from "next/link";

export default function Products({ products }){

    return (
        <>
            <h1>Products</h1>
            {
                products.map(product =>
                    <div key={product.id}>
                        <Link href={`/products/${product.id}`}>
                            <span>{product.id} {product.title}</span>
                        </Link>
                        <hr/>
                    </div>
                )
            }
        </>
    )

}

export async function getStaticProps(){
    console.log('Generating / Regenaring ProductList')
    const response = await fetch('http://localhost:4000/products')
    const data = await response.json()

    return {
        props: {
            products: data
        },
        revalidate: 10
    }
}