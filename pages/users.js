import User from "../components/user";

export default function Users({ users }){

    console.log('users: ', users)

    return (
        <>
            { users.map(user => <User key={user.id} user={user}/>) }
        </>
    )
}

export async function getStaticProps() {
    const response = await fetch('https://jsonplaceholder.typicode.com/users')
    const data = await response.json()
    console.log('data: ', data)

    return {
        props: {
            users: data
        }
    }
}